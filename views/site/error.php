<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
?>
<!-- <div class="admin-error">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-danger">
        <?= nl2br(Html::encode($message)) ?>
    </div>

    <p>
        The above error occurred while the Web server was processing your request.
    </p>
    <p>
        Please contact us if you think this is a server error. Thank you.
        testing
        hkjhkhjkhjkhjkhjhj
    </p>

</div> -->

<!-- =================== 404 container========== -->
        <section class="error_page_container">
            <div class="container">
                <div class="row">
                    <div class="pull-center title_holder">
                        <h2><span><?= Html::encode($this->title) ?></h2>
                        <p>We’re sorry but we can’t seem to find the page you requested. <br>
                        This might be because you have typed the web address incorrectly.</p>
                        <a href="../web/index.php">Go Home Page <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>
        </section>
<!-- =================== /404 container========== -->
