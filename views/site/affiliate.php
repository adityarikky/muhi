<?php
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = 'Download';
$this->params['breadcrumbs'][] = $this->title;
?>


<!-- ======= Banner ======= -->
        <section class="p0 container-fluid banner about_banner">
            <div class="about_banner_opacity">
                <div class="container">
                    <div class="banner_info_about">
                        <h1>Download</h1>
                        <ul>
                            <li><a href="../web/index.php?r=site%2Findex">Home</a></li>
                            <li><i class="fa fa-angle-right"></i></li>
                            <li>Download</li>
                        </ul>
                        
                    </div> <!-- End Banner Info -->
                </div> <!-- End Container -->
            </div> <!-- End Banner_opacity -->
        </section> <!-- End Banner -->
<!-- ======= /Banner ======= -->

<!-- ======= Latest Work ========= -->
        <section class="latest_work latest_work_two">
            <div class="container latest_work_title">
                <h2>Our Latest Work</h2>
                <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi.</p>
            </div>
            <div class="work_gallery">
                <div class="container project_row">
                    <div class="row">
                        <div class="menu_list"> <!-- Menu -->
                            <ul class="p0 work_menu">
                                <li class="filter active" data-filter="all">All</li>
                                <li class="filter" data-filter=".insurance">Insurance plans</li>
                                <li class="filter" data-filter=".debit">Debit</li>
                                <li class="filter" data-filter=".invoice">Invoice</li>
                                <li class="filter" data-filter=".report">Annual Report</li>
                            </ul>
                        </div>
                        <div id="mixitup_list">
                            <div class="work_img_two mix debit report">
                                <div style="display:inline-block; padding:0px;" class="mask_holder"><img class="img-responsive" src="images/pro/a.jpg" alt="image" title="Anual Company Growth">
                                    <div class="gallery_mask_hover">
                                        <a href="h" class="fancybox"><span>Anual Company Growth</span></a>
                                    </div>
                                </div>
                                <a href="images/pro/a.jpg" class="fancybox">Anual Company Growth <br><span>Debit/Invoicing</span></a>
                            </div>
                            <div class="work_img_two mix insurance debit report">
                                <div style="display:inline-block; padding:0px;" class="mask_holder"><img class="img-responsive" src="images/pro/b.jpg" alt="image" title="Anual Company Growth">
                                    <div class="gallery_mask_hover">
                                        <a href="images/pro/b.jpg" class="fancybox"><span>Anual Company Growth</span></a>
                                    </div>
                                </div>
                                <a href="images/pro/b.jpg" class="fancybox">Estelle Solution <br><span>Debit/Invoicing</span></a>
                            </div>
                            <div class="work_img_two mix insurance report invoice">
                                <div style="display:inline-block; padding:0px;" class="mask_holder"><img class="img-responsive" src="images/pro/c.jpg" alt="image" title="Anual Company Growth">
                                    <div class="gallery_mask_hover">
                                        <a href="images/pro/c.jpg" class="fancybox"><span>Anual Company Growth</span></a>
                                    </div>
                                </div>
                                <a href="images/pro/c.jpg" class="fancybox">Estelle Solution <br><span>Debit/Invoicing</span></a>
                            </div>
                            <div class="work_img_two mix insurance invoice">
                                <div style="display:inline-block; padding:0px;" class="mask_holder"><img class="img-responsive" src="images/pro/d.jpg" alt="image" title="Anual Company Growth">
                                    <div class="gallery_mask_hover">
                                        <a href="images/pro/d.jpg" class="fancybox"><span>Anual Company Growth</span></a>
                                    </div>
                                </div>
                                <a href="images/pro/d.jpg" class="fancybox">Anual Company Growth <br><span>Debit/Invoicing</span></a>
                            </div>
                            <div class="work_img_two mix debit invoice report">
                                <div style="display:inline-block; padding:0px;" class="mask_holder"><img class="img-responsive" src="images/pro/e.jpg" alt="image" title="Anual Company Growth">
                                    <div class="gallery_mask_hover">
                                        <a href="images/pro/e.jpg" class="fancybox"><span>Anual Company Growth</span></a>
                                    </div>
                                </div>
                                <a href="images/pro/e.jpg" class="fancybox">Anual Company Growth <br><span>Debit/Invoicing</span></a>
                            </div>
                            <div class="work_img_two mix insurance invoice">
                                <div style="display:inline-block; padding:0px;" class="mask_holder"><img class="img-responsive" src="images/pro/f.jpg" alt="image" title="Anual Company Growth">
                                    <div class="gallery_mask_hover">
                                        <a href="images/pro/f.jpg" class="fancybox"><span>Anual Company Growth</span></a>
                                    </div>
                                </div>
                                <a href="images/pro/f.jpg" class="fancybox">Estelle Solution <br><span>Debit/Invoicing</span></a>
                            </div>
                            <div class="work_img_two mix debit report">
                                <div style="display:inline-block; padding:0px;" class="mask_holder"><img class="img-responsive" src="images/pro/g.jpg" alt="image" title="Anual Company Growth">
                                    <div class="gallery_mask_hover">
                                        <a href="images/pro/g.jpg" class="fancybox"><span>Anual Company Growth</span></a>
                                    </div>
                                </div>
                                <a href="images/pro/g.jpg" class="fancybox">Estelle Solution <br><span>Debit/Invoicing</span></a>
                            </div>
                            <div class="work_img_two mix debit insurance report">
                                <div style="display:inline-block; padding:0px;" class="mask_holder"><img class="img-responsive" src="images/pro/h.jpg" alt="image" title="Anual Company Growth">
                                    <div class="gallery_mask_hover">
                                        <a href="images/pro/h.jpg" class="fancybox"><span>Anual Company Growth</span></a>
                                    </div>
                                </div>
                                <a href="images/pro/h.jpg" class="fancybox">Anual Company Growth <br><span>Debit/Invoicing</span></a>
                            </div>
                            <div class="work_img_two mix debit report invoice">
                                <div style="display:inline-block; padding:0px;" class="mask_holder"><img class="img-responsive" src="images/pro/i.jpg" alt="image" title="Anual Company Growth">
                                    <div class="gallery_mask_hover">
                                        <a href="images/pro/i.jpg" class="fancybox"><span>Anual Company Growth</span></a>
                                    </div>
                                </div>
                                <a href="images/pro/i.jpg" class="fancybox">Anual Company Growth <br><span>Debit/Invoicing</span></a>
                            </div>
                        </div>
                    </div> <!-- End row -->
                </div> <!-- End project_row -->
            </div> <!-- End work_gallery -->
        </section> <!-- End latest_work -->
<!-- ======= /Latest Work ========= -->