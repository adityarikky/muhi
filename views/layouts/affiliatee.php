<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.css" media="screen">
        
        <!-- Custom Css -->
        <link rel="stylesheet" type="text/css" href="css/custom/style.css">
        <link rel="stylesheet" type="text/css" href="css/responsive/responsive.css">
</head>
<body>
<?php
$pengaturan = app\models\Pengaturan::find()
                    ->all();
?>
<?php $this->beginBody() ?>
    <div class="wrap">
        <?php
            NavBar::begin([
                'brandLabel' => '<img style="width:110px" src="logo/logo_full.png"> ',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-default navbar-fixed-top',
                ],
            ]);
            
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav'],
                'items' => [
                        ['label' => 'Sarana berbagi tutorial animasi sederhana'],
                     ],
                ]);
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => [
                    ['label' => 'Home', 'url' => ['/site/index']],
                    ['label' => 'Affiliate','items'=>[
                            ['label'=>'SMA MUH 1 Yogyakarta','url' => 'http://muhi.gamatutor.id'],
                            ['label'=>'more','url' => ['/site/affiliate']]
                        ]],
                    ['label' => 'Download', 'url' => ['/site/download']],
                    ['label' => 'About', 'url' => ['/site/about']],
		    ['label' => 'Help', 'url' => ['/site/help']],

                    ['label' => 'Contact', 'url' => ['/site/contact']],
                    Yii::$app->user->isGuest ?
                        ['label' => 'Member','items'=>[
                            ['label'=>'Masuk','url' => ['/site/login']],
                            ['label'=>'Daftar','url' => ['/site/signup']]
                        ]] :
                        ['label' => 'Member','items'=>[
                            ['label'=>'Upload','url' => ['/tutorial/index']],
                            ['label' => 'Logout (' . Yii::$app->user->identity->username . ')',
                                'url' => ['/site/logout'],
                                'linkOptions' => ['data-method' => 'post']],
                            ['label'=>'Setting','url' => ['/site/setting']]
                        ]]
                ],
            ]);
            NavBar::end();
        ?>

        
            <?= $content ?>
        
    </div>

    <footer class="footer">
        <div class="container">
            <p class="pull-left">&copy; Make Meaning <?= date('Y') ?></p>
            <p class="pull-right"><?= Yii::powered() ?></p>
        </div>
    </footer>
    <!-- ============= /Footer =============== -->

        <!-- Js File -->

        <!-- j Query -->
                <script type="text/javascript" src="js/jquery-2.1.4.js"></script>
        <script type="text/javascript" src="js/jquery.mCustomScrollbar.concat.min.js"></script>
        <script type="text/javascript" src="js/jquery.bxslider.min.js"></script>
        <script src="js/revolution-slider/jquery.themepunch.tools.min.js"></script> <!-- Revolution Slider Tools -->
        <script src="js/revolution-slider/jquery.themepunch.revolution.min.js"></script> <!-- Revolution Slider -->
        <script type="text/javascript" src="js/revolution-slider/extensions/revolution.extension.actions.min.js"></script>
        <script type="text/javascript" src="js/revolution-slider/extensions/revolution.extension.carousel.min.js"></script>
        <script type="text/javascript" src="js/revolution-slider/extensions/revolution.extension.kenburn.min.js"></script>
        <script type="text/javascript" src="js/revolution-slider/extensions/revolution.extension.layeranimation.min.js"></script>
        <script type="text/javascript" src="js/revolution-slider/extensions/revolution.extension.migration.min.js"></script>
        <script type="text/javascript" src="js/revolution-slider/extensions/revolution.extension.navigation.min.js"></script>
        <script type="text/javascript" src="js/revolution-slider/extensions/revolution.extension.parallax.min.js"></script>
        <script type="text/javascript" src="js/revolution-slider/extensions/revolution.extension.slideanims.min.js"></script>
        <script type="text/javascript" src="js/revolution-slider/extensions/revolution.extension.video.min.js"></script>

        <!-- Bootstrap JS -->
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/jquery.appear.js"></script>
        <script type="text/javascript" src="js/jquery.countTo.js"></script>
        <script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
        <!-- Custom & Vendor js -->
        <script type="text/javascript" src="js/custom.js"></script>
        <script type="text/javascript" src="js/jquery.mixitup.min.js"></script>
        <script src="js/jquery.sliphover.min.js"></script>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
