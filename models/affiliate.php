<?php
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = 'Affiliate';
$this->params['breadcrumbs'][] = $this->title;
?>


<!-- ======= Latest Work ========= -->
        <section class="latest_work latest_work_two">
            <div class="container latest_work_title">
                <h1>Affiliate</h1>
                <h2>Affiliate merupakan program yang memberikan kebebasan kepada penguna untuk menerapkan sistem pembelajaran gamatutor.id. Tertarik ingin mencoba? Silahkan Hubungi Kami</h2>
            </div>
            <div class="work_gallery">
                <div class="container project_row">
                    <div class="row">
                        <div class="menu_list"> <!-- Menu -->
                            <ul class="p0 work_menu">
                                <li class="filter active" data-filter="all">All</li>
                                <li class="filter" data-filter=".sma">SMA</li>
                               <!--  <li class="filter" data-filter=".UNIVERSITAS">Insurance plans</li> -->
                                
                            </ul>
                        </div>
                        <div id="mixitup_list">
                            <div class="work_img_two mix sma report">
                            <!-- pastikan gambar jpg dengan ukuran lebar 389 tinggi 282 pixels -->
                                <div style="display:inline-block; padding:0px;" class="mask_holder"><img class="img-responsive" src="uploads/affiliate/muhi.jpg" alt="image" title="Anual Company Growth">
                                    <div class="gallery_mask_hover">
                                        <a href="http://muhi.gamatutor.id" class="fancybox"><span>SMA MUHAMMADIYAH 1 YOGYAKARTA</span></a>
                                    </div>
                                </div>
                                <a href="http://muhi.gamatutor.id" class="fancybox">SMA MUHAMMADIYAH 1 <br><span>YOGYAKARTA</span></a>
                            </div>
                            
                        </div>
                    </div> <!-- End row -->
                </div> <!-- End project_row -->
            </div> <!-- End work_gallery -->
        </section> <!-- End latest_work -->
<!-- ======= /Latest Work ========= -->
