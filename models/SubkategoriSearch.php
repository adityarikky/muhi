<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Subkategori;

/**
 * SubkategoriSearch represents the model behind the search form about `app\models\Subkategori`.
 */
class SubkategoriSearch extends Subkategori
{
    /**
     * @inheritdoc
     */
    public $globalSearch;
    public function rules()
    {
         
        return [
            [['id', 'kategori_id'], 'integer'],
            [['nama', 'deskripsi', 'icon','globalSearch'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Subkategori::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'kategori_id' => $this->kategori_id,
        ]);

        $query->orFilterWhere(['like', 'nama', $this->globalSearch])
            ->orFilterWhere(['like', 'deskripsi', $this->globalSearch])
            ->orFilterWhere(['like', 'icon', $this->globalSearch]);

        return $dataProvider;
    }
}
