-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 15, 2016 at 02:58 PM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gamatutor`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_assignment`
--

CREATE TABLE IF NOT EXISTS `auth_assignment` (
  `item_name` varchar(64) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('admin', 6, NULL),
('admin', 7, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item`
--

CREATE TABLE IF NOT EXISTS `auth_item` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `rule_name` varchar(64) DEFAULT NULL,
  `data` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('admin', 2, 'super admin can edit, update, and delete enything row in datagrid', NULL, NULL, NULL, NULL),
('student', 1, NULL, NULL, NULL, NULL, NULL),
('teacher', 1, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item_child`
--

CREATE TABLE IF NOT EXISTS `auth_item_child` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('admin', 'student'),
('teacher', 'student'),
('admin', 'teacher');

-- --------------------------------------------------------

--
-- Table structure for table `auth_rule`
--

CREATE TABLE IF NOT EXISTS `auth_rule` (
  `name` varchar(64) NOT NULL,
  `data` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `create_time`, `update_time`) VALUES
(1, 'Pendidikan Agama', NULL, NULL),
(2, 'Pendidikan Pancasila', NULL, NULL),
(3, 'Matematika', NULL, NULL),
(4, 'Sejarah Indonesia', NULL, NULL),
(5, 'Bahasa Indonesia', NULL, NULL),
(6, 'Bahasa Inggris', NULL, NULL),
(7, 'Seni Budaya', NULL, NULL),
(8, 'Penjaskes', NULL, NULL),
(9, 'Biologi', NULL, NULL),
(10, 'Fisika', NULL, NULL),
(11, 'Kimia', NULL, NULL),
(12, 'Matematika', NULL, NULL),
(13, 'Geografi', NULL, NULL),
(14, 'Sejarah', NULL, NULL),
(15, 'Sosiologi dan Anthropologi', NULL, NULL),
(16, 'Bahasa dan Sastra Arab', NULL, NULL),
(17, 'Bahasa dan Sastra Mandarin', NULL, NULL);

-- --------------------------------------------------------

--
-- Stand-in structure for view `jumlah_post`
--
CREATE TABLE IF NOT EXISTS `jumlah_post` (
`name` varchar(255)
,`count` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `jumlah_post_byid`
--
CREATE TABLE IF NOT EXISTS `jumlah_post_byid` (
`username` varchar(255)
,`count` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `jumlah_tutorial`
--
CREATE TABLE IF NOT EXISTS `jumlah_tutorial` (
`nama` varchar(250)
,`count` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `jumlah_tutorial_byid`
--
CREATE TABLE IF NOT EXISTS `jumlah_tutorial_byid` (
`username` varchar(255)
,`count` bigint(21)
);

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE IF NOT EXISTS `kategori` (
  `id` int(11) NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `deskripsi` text
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id`, `name`, `deskripsi`) VALUES
(1, 'SMA', NULL);

-- --------------------------------------------------------

--
-- Stand-in structure for view `pdf_post`
--
CREATE TABLE IF NOT EXISTS `pdf_post` (
`name` varchar(128)
,`category` varchar(255)
,`status` varchar(11)
,`realname` varchar(255)
,`nis` varchar(40)
,`email` varchar(255)
,`telepon` varchar(255)
,`user_id` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `pdf_tutorial`
--
CREATE TABLE IF NOT EXISTS `pdf_tutorial` (
`name` varchar(250)
,`category` varchar(250)
,`status` varchar(15)
,`realname` varchar(255)
,`nis` varchar(40)
,`email` varchar(255)
,`telepon` varchar(255)
,`user_id` int(11)
);

-- --------------------------------------------------------

--
-- Table structure for table `pengaturan`
--

CREATE TABLE IF NOT EXISTS `pengaturan` (
  `id` int(11) NOT NULL,
  `top_bar_status` varchar(300) NOT NULL,
  `welcome_status` varchar(300) NOT NULL,
  `alamat` varchar(300) NOT NULL,
  `kodepos` varchar(300) NOT NULL,
  `telp` varchar(300) NOT NULL,
  `email` varchar(300) NOT NULL,
  `deskripsi` varchar(300) NOT NULL,
  `facebook` varchar(300) NOT NULL,
  `twitter` varchar(300) NOT NULL,
  `google_plus` varchar(300) NOT NULL,
  `linked_in` varchar(300) NOT NULL,
  `skype` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengaturan`
--

INSERT INTO `pengaturan` (`id`, `top_bar_status`, `welcome_status`, `alamat`, `kodepos`, `telp`, `email`, `deskripsi`, `facebook`, `twitter`, `google_plus`, `linked_in`, `skype`) VALUES
(1, 'Belajar dan Mencoba, why not!', 'Mungkin saja anda mengalami kesulitan saat belajar, tapi anda akan menerima kemudahan setelah anda memahami dan menerapkan apa yang anda telah pelajari tadi.', 'Jl. Gotongroyong II, Petinggen ', '55241 ', '+62274-563739', 'mail@elearning.com', 'Sekolah Menengah Atas Muhammadiyah 1 Yogyakarta atau yang biasa dikenal dengan nama SMA MUHI adalah salah satu sekolah swasta milik persyarikatan Muhammadiyah yang berada di Provinsi D.I. Yogyakarta.', 'https://www.facebook.com/SMA-Muhammadiyah-1-Yogyakarta-253890074660283/', 'https://twitter.com/smamuhi_jogja', 'https://www.google.com/maps/place/SMA+Muhammadiyah+1+Yogyakarta/data=!4m2!3m1!1s0x2e7a5846856805e9:0x4ff6a092c652d8f3?gl=ID&hl=in', 'https://www.google.com/maps/place/SMA+Muhammadiyah+1+Yogyakarta/data=!4m2!3m1!1s0x2e7a5846856805e9:0x4ff6a092c652d8f3?gl=ID&hl=in', 'https://www.facebook.com/SMA-Muhammadiyah-1-Yogyakarta-253890074660283/');

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE IF NOT EXISTS `post` (
  `id` int(11) NOT NULL,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `foto` text CHARACTER SET utf8 NOT NULL,
  `status` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`id`, `title`, `content`, `category_id`, `foto`, `status`, `create_time`, `update_time`, `user_id`) VALUES
(1, 'Program Linear', '<p><strong>Program Linear</strong>&nbsp;merupakan bagian dari matematika berupa pemecahan masalah pengoptimalan, yaitu memaksimumkan atau meminimumkan suatu fungsi linear yang bergantung pada kendala (batasan) linear. Kendala ini bisa berupa pertidaksamaan atau<a href="http://www.sekolahmatematika.com/sistem-persamaan-linear/" style="padding: 0px; margin: 0px; outline: none; list-style: none; border: 0px none; box-sizing: border-box; color: rgb(0, 0, 255); text-decoration: none; transition: all 0.2s ease-in-out;" target="_blank">persamaan linear</a>.</p>\r\n\r\n<p>Sebagai contoh, kita ingin memaksimalkan nilai dari&nbsp;<img alt="x+y" class="latex tie-appear" src="http://s0.wp.com/latex.php?latex=x%2By&amp;bg=f9f9f9&amp;fg=000000&amp;s=0" style="border:0px; box-sizing:border-box; display:inline; height:auto; list-style:none; margin:0px; max-width:100%; opacity:1; outline:none; padding:0px; transition:all 0.4s ease-in-out; vertical-align:middle" title="x+y" />&nbsp;dengan kendala:</p>\r\n\r\n<p style="text-align:center"><img alt="3x + y \\le 6 \\newline \\newline 2x+4y \\le 8 \\newline \\newline x+y \\ge 1" class="latex tie-appear" src="http://s0.wp.com/latex.php?latex=3x+%2B+y+%5Cle+6+%5Cnewline+%5Cnewline+2x%2B4y+%5Cle+8+%5Cnewline+%5Cnewline+x%2By+%5Cge+1&amp;bg=f9f9f9&amp;fg=000000&amp;s=0" style="border:0px; box-sizing:border-box; display:inline; height:auto; list-style:none; margin:0px; max-width:100%; opacity:1; outline:none; padding:0px; transition:all 0.4s ease-in-out; vertical-align:middle" title="3x + y \\le 6 \\newline \\newline 2x+4y \\le 8 \\newline \\newline x+y \\ge 1" /></p>\r\n\r\n<p>Jadi, nilai&nbsp;<img alt="x" class="latex tie-appear" src="http://s0.wp.com/latex.php?latex=x&amp;bg=f9f9f9&amp;fg=000000&amp;s=0" style="border:0px; box-sizing:border-box; display:inline; height:auto; list-style:none; margin:0px; max-width:100%; opacity:1; outline:none; padding:0px; transition:all 0.4s ease-in-out; vertical-align:middle" title="x" />&nbsp;dan&nbsp;<img alt="y" class="latex tie-appear" src="http://s0.wp.com/latex.php?latex=y&amp;bg=f9f9f9&amp;fg=000000&amp;s=0" style="border:0px; box-sizing:border-box; display:inline; height:auto; list-style:none; margin:0px; max-width:100%; opacity:1; outline:none; padding:0px; transition:all 0.4s ease-in-out; vertical-align:middle" title="y" />&nbsp;yang memaksimumkan nilai&nbsp;<img alt="x+y" class="latex tie-appear" src="http://s0.wp.com/latex.php?latex=x%2By&amp;bg=f9f9f9&amp;fg=000000&amp;s=0" style="border:0px; box-sizing:border-box; display:inline; height:auto; list-style:none; margin:0px; max-width:100%; opacity:1; outline:none; padding:0px; transition:all 0.4s ease-in-out; vertical-align:middle" title="x+y" />&nbsp;harus memenuhi ketiga pertidaksamaan (kendala) di atas.</p>\r\n\r\n<p>(Lihat juga:&nbsp;<a href="http://www.sekolahmatematika.com/fungsi-kuadrat/" style="padding: 0px; margin: 0px; outline: none; list-style: none; border: 0px none; box-sizing: border-box; color: rgb(0, 0, 255); text-decoration: none; transition: all 0.2s ease-in-out;" target="_blank">Fungsi Kuadrat</a>)</p>\r\n\r\n<p>Untuk itu, agar kamu memahami materi program linear, kamu harus memahami terlebih dahulu materi&nbsp;<a href="http://www.sekolahmatematika.com/persamaan-garis-lurus/" style="padding: 0px; margin: 0px; outline: none; list-style: none; border: 0px none; box-sizing: border-box; color: rgb(0, 0, 255); text-decoration: none; transition: all 0.2s ease-in-out;" target="_blank">persamaan garis lurus</a>&nbsp;dan sistem pertidaksamaan linear.</p>\r\n\r\n<h2>Review Persamaan Garis Lurus</h2>\r\n\r\n<p>Persamaan garis yang melewati titik (0, a) dan (b, 0) adalah&nbsp;<img alt="ax + by = ab" class="latex tie-appear" src="http://s0.wp.com/latex.php?latex=ax+%2B+by+%3D+ab&amp;bg=f9f9f9&amp;fg=000000&amp;s=0" style="border:0px; box-sizing:border-box; display:inline; height:auto; list-style:none; margin:0px; max-width:100%; opacity:1; outline:none; padding:0px; transition:all 0.4s ease-in-out; vertical-align:middle" title="ax + by = ab" /></p>\r\n\r\n<p>Contoh: Persamaan garis yang melewati titik A (0,3) dan (5, 0) adalah&nbsp;<img alt="3x + 5y = 15" class="latex tie-appear" src="http://s0.wp.com/latex.php?latex=3x+%2B+5y+%3D+15&amp;bg=f9f9f9&amp;fg=000000&amp;s=0" style="border:0px; box-sizing:border-box; display:inline; height:auto; list-style:none; margin:0px; max-width:100%; opacity:1; outline:none; padding:0px; transition:all 0.4s ease-in-out; vertical-align:middle" title="3x + 5y = 15" /></p>\r\n\r\n<p>Persamaan garis yang melewati titik&nbsp;<img alt="x_1, y_1" class="latex tie-appear" src="http://s0.wp.com/latex.php?latex=x_1%2C+y_1&amp;bg=f9f9f9&amp;fg=000000&amp;s=0" style="border:0px; box-sizing:border-box; display:inline; height:auto; list-style:none; margin:0px; max-width:100%; opacity:1; outline:none; padding:0px; transition:all 0.4s ease-in-out; vertical-align:middle" title="x_1, y_1" />&nbsp;dan&nbsp;<img alt="x_2, y_2" class="latex tie-appear" src="http://s0.wp.com/latex.php?latex=x_2%2C+y_2&amp;bg=f9f9f9&amp;fg=000000&amp;s=0" style="border:0px; box-sizing:border-box; display:inline; height:auto; list-style:none; margin:0px; max-width:100%; opacity:1; outline:none; padding:0px; transition:all 0.4s ease-in-out; vertical-align:middle" title="x_2, y_2" />&nbsp;adalah:</p>\r\n\r\n<p style="text-align:center"><img alt="\\frac{x - x_1}{x_2 - x_1} = \\frac{y - y_1}{y_2 - y_1}" class="latex tie-appear" src="http://s0.wp.com/latex.php?latex=%5Cfrac%7Bx+-+x_1%7D%7Bx_2+-+x_1%7D+%3D+%5Cfrac%7By+-+y_1%7D%7By_2+-+y_1%7D&amp;bg=f9f9f9&amp;fg=000000&amp;s=0" style="border:0px; box-sizing:border-box; display:inline; height:auto; list-style:none; margin:0px; max-width:100%; opacity:1; outline:none; padding:0px; transition:all 0.4s ease-in-out; vertical-align:middle" title="\\frac{x - x_1}{x_2 - x_1} = \\frac{y - y_1}{y_2 - y_1}" /></p>\r\n\r\n<p>Contoh: Persamaan garis yang melewati titik A (2, 4) dan B (3, 5) adalah:</p>\r\n\r\n<p style="text-align:center"><img alt="\\frac{x - 2}{3 - 2} = \\frac{y - 4}{5 - 4}" class="latex tie-appear" src="http://s0.wp.com/latex.php?latex=%5Cfrac%7Bx+-+2%7D%7B3+-+2%7D+%3D+%5Cfrac%7By+-+4%7D%7B5+-+4%7D&amp;bg=f9f9f9&amp;fg=000000&amp;s=0" style="border:0px; box-sizing:border-box; display:inline; height:auto; list-style:none; margin:0px; max-width:100%; opacity:1; outline:none; padding:0px; transition:all 0.4s ease-in-out; vertical-align:middle" title="\\frac{x - 2}{3 - 2} = \\frac{y - 4}{5 - 4}" /></p>\r\n\r\n<p style="text-align:center"><img alt="x - 2 = y - 4" class="latex tie-appear" src="http://s0.wp.com/latex.php?latex=x+-+2+%3D+y+-+4&amp;bg=f9f9f9&amp;fg=000000&amp;s=0" style="border:0px; box-sizing:border-box; display:inline; height:auto; list-style:none; margin:0px; max-width:100%; opacity:1; outline:none; padding:0px; transition:all 0.4s ease-in-out; vertical-align:middle" title="x - 2 = y - 4" /></p>\r\n\r\n<p style="text-align:center"><img alt="x - y = -2" class="latex tie-appear" src="http://s0.wp.com/latex.php?latex=x+-+y+%3D+-2&amp;bg=f9f9f9&amp;fg=000000&amp;s=0" style="border:0px; box-sizing:border-box; display:inline; height:auto; list-style:none; margin:0px; max-width:100%; opacity:1; outline:none; padding:0px; transition:all 0.4s ease-in-out; vertical-align:middle" title="x - y = -2" /></p>\r\n\r\n<h2>Sistem Pertidaksamaan Linear</h2>\r\n\r\n<p>Sistem pertidaksamaan linear merupakan gabungan dari beberapa pertidaksamaan linear. Pertidaksamaan linear pada topik program linear biasanya berupa pertidaksamaan yang terdiri dari 2 variabel, yaitu x dan y. Misalnya,&nbsp;<img alt="ax + by \\le c" class="latex tie-appear" src="http://s0.wp.com/latex.php?latex=ax+%2B+by+%5Cle+c&amp;bg=f9f9f9&amp;fg=000000&amp;s=0" style="border:0px; box-sizing:border-box; display:inline; height:auto; list-style:none; margin:0px; max-width:100%; opacity:1; outline:none; padding:0px; transition:all 0.4s ease-in-out; vertical-align:middle" title="ax + by \\le c" />.</p>\r\n\r\n<p>Pada soal program linear, terkadang bentuk pertidaksamaan tidak langsung dinyatakan dalam notasi variabel, tetapi melalui suatu bahasa atau pernyataan, sehingga perlu diterjemahkan ke bentuk pertidaksamaan linear biasa. Penerjemahan ini disebut dengan pemodelan matematika, dan sistem pertidaksamaan liner yang terbentuk disebut dengan&nbsp;<strong>model matematika</strong>.</p>\r\n\r\n<p>Himpunan penyelesaian pertidaksamaan ini dapat ditentukan dengan menggunakan metode grafik dan uji titik.</p>\r\n\r\n<p>Misalnya kita ingin menggambar grafik&nbsp;<img alt="ax+by &lt;= c" class="latex tie-appear" src="http://s0.wp.com/latex.php?latex=ax%2Bby+%3C%3D+c&amp;bg=f9f9f9&amp;fg=000000&amp;s=0" style="border:0px; box-sizing:border-box; display:inline; height:auto; list-style:none; margin:0px; max-width:100%; opacity:1; outline:none; padding:0px; transition:all 0.4s ease-in-out; vertical-align:middle" title="ax+by &lt;= c" />. Langkah-langkahnya adalah:</p>\r\n\r\n<ol>\r\n	<li style="text-align:left">Gambarkan garis&nbsp;<img alt="ax+by=c" class="latex tie-appear" src="http://s0.wp.com/latex.php?latex=ax%2Bby%3Dc&amp;bg=f9f9f9&amp;fg=000000&amp;s=0" style="border:0px; box-sizing:border-box; display:inline; height:auto; list-style:none; margin:0px; max-width:100%; opacity:1; outline:none; padding:0px; transition:all 0.4s ease-in-out; vertical-align:middle" title="ax+by=c" />&nbsp;pada koordinat Cartesius.</li>\r\n	<li style="text-align:left">Pilih satu (sembarang) titik yang tidak terletak pada garis&nbsp;<img alt="ax+by=c" class="latex tie-appear" src="http://s0.wp.com/latex.php?latex=ax%2Bby%3Dc&amp;bg=f9f9f9&amp;fg=000000&amp;s=0" style="border:0px; box-sizing:border-box; display:inline; height:auto; list-style:none; margin:0px; max-width:100%; opacity:1; outline:none; padding:0px; transition:all 0.4s ease-in-out; vertical-align:middle" title="ax+by=c" />&nbsp;tersebut, lalu substitusikan nilai titik&nbsp;<img alt="(x, y)" class="latex tie-appear" src="http://s0.wp.com/latex.php?latex=%28x%2C+y%29&amp;bg=f9f9f9&amp;fg=000000&amp;s=0" style="border:0px; box-sizing:border-box; display:inline; height:auto; list-style:none; margin:0px; max-width:100%; opacity:1; outline:none; padding:0px; transition:all 0.4s ease-in-out; vertical-align:middle" title="(x, y)" />&nbsp;tersebut ke pertidaksaman&nbsp;<img alt="ax+by &lt;= c" class="latex tie-appear" src="http://s0.wp.com/latex.php?latex=ax%2Bby+%3C%3D+c&amp;bg=f9f9f9&amp;fg=000000&amp;s=0" style="border:0px; box-sizing:border-box; display:inline; height:auto; list-style:none; margin:0px; max-width:100%; opacity:1; outline:none; padding:0px; transition:all 0.4s ease-in-out; vertical-align:middle" title="ax+by &lt;= c" />. Untuk mempermudah perhitungan, ujilah pertidaksamaan tersebut pada titik O (0,0).\r\n	<ul>\r\n		<li style="text-align:left">Jika pertidaksamaan tersebut bernilai salah, maka himpunan penyelesaiannya adalah daerah yang tidak memuat titik tersebut, dengan batasnya adalah garis&nbsp;<img alt="ax+by=c" class="latex tie-appear" src="http://s0.wp.com/latex.php?latex=ax%2Bby%3Dc&amp;bg=f9f9f9&amp;fg=000000&amp;s=0" style="border:0px; box-sizing:border-box; display:inline; height:auto; list-style:none; margin:0px; max-width:100%; opacity:1; outline:none; padding:0px; transition:all 0.4s ease-in-out; vertical-align:middle" title="ax+by=c" /></li>\r\n		<li style="text-align:left">Jika pertidaksamaan tersebut bernilai benar, maka himpunan penyelesaiannya adalah daerah yang memuat titik tersebut, dengan batasnya adalah garis&nbsp;<img alt="ax+by=c" class="latex tie-appear" src="http://s0.wp.com/latex.php?latex=ax%2Bby%3Dc&amp;bg=f9f9f9&amp;fg=000000&amp;s=0" style="border:0px; box-sizing:border-box; display:inline; height:auto; list-style:none; margin:0px; max-width:100%; opacity:1; outline:none; padding:0px; transition:all 0.4s ease-in-out; vertical-align:middle" title="ax+by=c" /></li>\r\n	</ul>\r\n	</li>\r\n</ol>\r\n\r\n<p>Sedangkan himpunan penyelesaian sistem pertidaksamaan linearnya adalah irisan dari semua daerah himpunan penyelesaian pertidaksamaan linear tersebut.</p>\r\n\r\n<p>Contoh:</p>\r\n\r\n<p>Tentukanlah himpunan penyelesaian dari:</p>\r\n\r\n<p style="text-align:center"><img alt="3x+y \\le 15 \\newline \\newline x+2y \\le 10 \\newline \\newline x+y \\ge 5 \\newline \\newline x \\ge 0, y \\ge 0" class="latex tie-appear" src="http://s0.wp.com/latex.php?latex=3x%2By+%5Cle+15+%5Cnewline+%5Cnewline+x%2B2y+%5Cle+10+%5Cnewline+%5Cnewline+x%2By+%5Cge+5+%5Cnewline+%5Cnewline+x+%5Cge+0%2C+y+%5Cge+0&amp;bg=f9f9f9&amp;fg=000000&amp;s=0" style="border:0px; box-sizing:border-box; display:inline; height:auto; list-style:none; margin:0px; max-width:100%; opacity:1; outline:none; padding:0px; transition:all 0.4s ease-in-out; vertical-align:middle" title="3x+y \\le 15 \\newline \\newline x+2y \\le 10 \\newline \\newline x+y \\ge 5 \\newline \\newline x \\ge 0, y \\ge 0" /></p>\r\n\r\n<p>Jawab:</p>\r\n\r\n<p>Gambarkan terlebih dahulu grafik&nbsp;<img alt="3x+y=15" class="latex tie-appear" src="http://s0.wp.com/latex.php?latex=3x%2By%3D15&amp;bg=f9f9f9&amp;fg=000000&amp;s=0" style="border:0px; box-sizing:border-box; display:inline; height:auto; list-style:none; margin:0px; max-width:100%; opacity:1; outline:none; padding:0px; transition:all 0.4s ease-in-out; vertical-align:middle" title="3x+y=15" />,&nbsp;<img alt="x+2y=10" class="latex tie-appear" src="http://s0.wp.com/latex.php?latex=x%2B2y%3D10&amp;bg=f9f9f9&amp;fg=000000&amp;s=0" style="border:0px; box-sizing:border-box; display:inline; height:auto; list-style:none; margin:0px; max-width:100%; opacity:1; outline:none; padding:0px; transition:all 0.4s ease-in-out; vertical-align:middle" title="x+2y=10" />, dan&nbsp;<img alt="x+y=5" class="latex tie-appear" src="http://s0.wp.com/latex.php?latex=x%2By%3D5&amp;bg=f9f9f9&amp;fg=000000&amp;s=0" style="border:0px; box-sizing:border-box; display:inline; height:auto; list-style:none; margin:0px; max-width:100%; opacity:1; outline:none; padding:0px; transition:all 0.4s ease-in-out; vertical-align:middle" title="x+y=5" />&nbsp;pada koordinat Cartesius. Perhatikan grafik di bawah ini:</p>\r\n\r\n<p><img alt="program linear" class="aligncenter tie-appear" src="http://www.sekolahmatematika.com/wp-content/uploads/2015/08/contoh-program-linear.jpg" style="border:0px none; box-sizing:border-box; clear:both; display:block; height:auto; list-style:none; margin:5px auto; max-width:100%; opacity:1; outline:none; padding:0px; transition:all 0.4s ease-in-out; vertical-align:middle" /></p>\r\n\r\n<p>Kemudian, uji masing-masing pertidaksamaan pada titik O (0,0), untuk menentukan daerah himpunan penyelesaian setiap pertidaksamaan, dan perhatikan&nbsp; daerah irisannya. Maka didapat himpunan penyelesaiannya seperti pada grafik berikut:</p>\r\n\r\n<p><img alt="contoh soal program linear" class="aligncenter tie-appear" src="http://www.sekolahmatematika.com/wp-content/uploads/2015/08/contoh-soal-program-linear.jpg" style="border:0px none; box-sizing:border-box; clear:both; display:block; height:auto; list-style:none; margin:5px auto; max-width:100%; opacity:1; outline:none; padding:0px; transition:all 0.4s ease-in-out; vertical-align:middle" /></p>\r\n\r\n<h2>Nilai Optimum Fungsi Objektif Program Linear</h2>\r\n\r\n<p>Pada program linear, fungsi objektif merupakan suatu fungsi&nbsp;<img alt="f(x,y) = ax + by" class="latex tie-appear" src="http://s0.wp.com/latex.php?latex=f%28x%2Cy%29+%3D+ax+%2B+by&amp;bg=f9f9f9&amp;fg=000000&amp;s=0" style="border:0px; box-sizing:border-box; display:inline; height:auto; list-style:none; margin:0px; max-width:100%; opacity:1; outline:none; padding:0px; transition:all 0.4s ease-in-out; vertical-align:middle" title="f(x,y) = ax + by" />&nbsp;yang hendak ditentukan nilai optimumnya (maksimum atau minimum).</p>\r\n\r\n<p>Nilai optimum suatu fungsi objektif dapat ditentukan dengan menggunakan: (1) metode garis selidik (membuat persamaan garis selidik) dan menggeser-geser garis selidik di daerah himpunan penyelesaian, atau (2). metode pengujian titik sudut (menguji nilai&nbsp;<img alt="(x, y)" class="latex tie-appear" src="http://s0.wp.com/latex.php?latex=%28x%2C+y%29&amp;bg=f9f9f9&amp;fg=000000&amp;s=0" style="border:0px; box-sizing:border-box; display:inline; height:auto; list-style:none; margin:0px; max-width:100%; opacity:1; outline:none; padding:0px; transition:all 0.4s ease-in-out; vertical-align:middle" title="(x, y)" />&nbsp;titik sudut dan mensubstitusikannya pada fungsi objektif program linear.</p>\r\n\r\n<p>Titik sudut merupakan titik perpotongan masing-masing pertidaksamaan linear. Koordinat titik sudut dapat dihitung dengan menggunakan sistem persamaan linear dua variabel (SPLDV).</p>\r\n\r\n<p>Menurut penulis, cara yang paling mudah adalah dengan menggunakan metode pengujian titik sudut, di mana kita tinggal mensubstitusi nilai titik sudut sistem pertidaksamaan linear pada grafik koordinat Cartesius.</p>\r\n\r\n<h3>Contoh Soal Program Linear</h3>\r\n\r\n<p>Nilai maksimum dari&nbsp;<img alt="20x + 8y" class="latex tie-appear" src="http://s0.wp.com/latex.php?latex=20x+%2B+8y&amp;bg=f9f9f9&amp;fg=000000&amp;s=0" style="border:0px; box-sizing:border-box; display:inline; height:auto; list-style:none; margin:0px; max-width:100%; opacity:1; outline:none; padding:0px; transition:all 0.4s ease-in-out; vertical-align:middle" title="20x + 8y" />&nbsp;untuk&nbsp;<img alt="x" class="latex tie-appear" src="http://s0.wp.com/latex.php?latex=x&amp;bg=f9f9f9&amp;fg=000000&amp;s=0" style="border:0px; box-sizing:border-box; display:inline; height:auto; list-style:none; margin:0px; max-width:100%; opacity:1; outline:none; padding:0px; transition:all 0.4s ease-in-out; vertical-align:middle" title="x" />&nbsp;dan&nbsp;<img alt="y" class="latex tie-appear" src="http://s0.wp.com/latex.php?latex=y&amp;bg=f9f9f9&amp;fg=000000&amp;s=0" style="border:0px; box-sizing:border-box; display:inline; height:auto; list-style:none; margin:0px; max-width:100%; opacity:1; outline:none; padding:0px; transition:all 0.4s ease-in-out; vertical-align:middle" title="y" />&nbsp;yang memenuhi&nbsp;<img alt="x+y \\ge 20" class="latex tie-appear" src="http://s0.wp.com/latex.php?latex=x%2By+%5Cge+20&amp;bg=f9f9f9&amp;fg=000000&amp;s=0" style="border:0px; box-sizing:border-box; display:inline; height:auto; list-style:none; margin:0px; max-width:100%; opacity:1; outline:none; padding:0px; transition:all 0.4s ease-in-out; vertical-align:middle" title="x+y \\ge 20" />,&nbsp;<img alt="2x + y \\le 48 " class="latex tie-appear" src="http://s0.wp.com/latex.php?latex=2x+%2B+y+%5Cle+48+&amp;bg=f9f9f9&amp;fg=000000&amp;s=0" style="border:0px; box-sizing:border-box; display:inline; height:auto; list-style:none; margin:0px; max-width:100%; opacity:1; outline:none; padding:0px; transition:all 0.4s ease-in-out; vertical-align:middle" title="2x + y \\le 48 " />,&nbsp;<img alt="0 \\le x \\le 20" class="latex tie-appear" src="http://s0.wp.com/latex.php?latex=0+%5Cle+x+%5Cle+20&amp;bg=f9f9f9&amp;fg=000000&amp;s=0" style="border:0px; box-sizing:border-box; display:inline; height:auto; list-style:none; margin:0px; max-width:100%; opacity:1; outline:none; padding:0px; transition:all 0.4s ease-in-out; vertical-align:middle" title="0 \\le x \\le 20" />, dan&nbsp;<img alt="0 \\le y \\le 48" class="latex tie-appear" src="http://s0.wp.com/latex.php?latex=0+%5Cle+y+%5Cle+48&amp;bg=f9f9f9&amp;fg=000000&amp;s=0" style="border:0px; box-sizing:border-box; display:inline; height:auto; list-style:none; margin:0px; max-width:100%; opacity:1; outline:none; padding:0px; transition:all 0.4s ease-in-out; vertical-align:middle" title="0 \\le y \\le 48" />&nbsp;adalah &hellip;. (SPMB 2005)</p>\r\n\r\n<p>Jawab:</p>\r\n\r\n<p>Gambarkan pertidaksamaan tersebut ke dalam koordinat Cartesius, yaitu sbb:</p>\r\n\r\n<p><img alt="program model matematika" class="aligncenter tie-appear" src="http://www.sekolahmatematika.com/wp-content/uploads/2015/08/program-model-matematika.jpg" style="border:0px none; box-sizing:border-box; clear:both; display:block; height:auto; list-style:none; margin:5px auto; max-width:100%; opacity:1; outline:none; padding:0px; transition:all 0.4s ease-in-out; vertical-align:middle" /></p>\r\n\r\n<p>Kemudian tentukan daerah himpunan penyelesaiannya, yaitu irisan dari setiap pertidaksamaan. Yaitu seperti pada grafik di bawah ini:</p>\r\n\r\n<p><img alt="program contoh soal" class="aligncenter tie-appear" src="http://www.sekolahmatematika.com/wp-content/uploads/2015/08/program-contoh-soal.jpg" style="border:0px none; box-sizing:border-box; clear:both; display:block; height:auto; list-style:none; margin:5px auto; max-width:100%; opacity:1; outline:none; padding:0px; transition:all 0.4s ease-in-out; vertical-align:middle" /></p>\r\n\r\n<p>Titik potong haris&nbsp;<img alt="x = 20" class="latex tie-appear" src="http://s0.wp.com/latex.php?latex=x+%3D+20&amp;bg=f9f9f9&amp;fg=000000&amp;s=0" style="border:0px; box-sizing:border-box; display:inline; height:auto; list-style:none; margin:0px; max-width:100%; opacity:1; outline:none; padding:0px; transition:all 0.4s ease-in-out; vertical-align:middle" title="x = 20" />&nbsp;dan&nbsp;<img alt="2x + y = 48" class="latex tie-appear" src="http://s0.wp.com/latex.php?latex=2x+%2B+y+%3D+48&amp;bg=f9f9f9&amp;fg=000000&amp;s=0" style="border:0px; box-sizing:border-box; display:inline; height:auto; list-style:none; margin:0px; max-width:100%; opacity:1; outline:none; padding:0px; transition:all 0.4s ease-in-out; vertical-align:middle" title="2x + y = 48" />&nbsp;adalah&nbsp;<img alt="(20, 8)" class="latex tie-appear" src="http://s0.wp.com/latex.php?latex=%2820%2C+8%29&amp;bg=f9f9f9&amp;fg=000000&amp;s=0" style="border:0px; box-sizing:border-box; display:inline; height:auto; list-style:none; margin:0px; max-width:100%; opacity:1; outline:none; padding:0px; transition:all 0.4s ease-in-out; vertical-align:middle" title="(20, 8)" /></p>\r\n\r\n<p>Uji titik sudut:</p>\r\n\r\n<p><img alt="(0, 48) \\longrightarrow f(x,y) = 20x + 8y = 20 \\cdot 0 + 8 \\cdot 48 = 384" class="latex tie-appear" src="http://s0.wp.com/latex.php?latex=%280%2C+48%29+%5Clongrightarrow+f%28x%2Cy%29+%3D+20x+%2B+8y+%3D+20+%5Ccdot+0+%2B+8+%5Ccdot+48+%3D+384&amp;bg=f9f9f9&amp;fg=000000&amp;s=0" style="border:0px; box-sizing:border-box; display:inline; height:auto; list-style:none; margin:0px; max-width:100%; opacity:1; outline:none; padding:0px; transition:all 0.4s ease-in-out; vertical-align:middle" title="(0, 48) \\longrightarrow f(x,y) = 20x + 8y = 20 \\cdot 0 + 8 \\cdot 48 = 384" /></p>\r\n\r\n<p><img alt="(20, 8) \\longrightarrow f(x,y) = 20x + 8y = 20 \\cdot 20 + 8 \\cdot 8 = 464" class="latex tie-appear" src="http://s0.wp.com/latex.php?latex=%2820%2C+8%29+%5Clongrightarrow+f%28x%2Cy%29+%3D+20x+%2B+8y+%3D+20+%5Ccdot+20+%2B+8+%5Ccdot+8+%3D+464&amp;bg=f9f9f9&amp;fg=000000&amp;s=0" style="border:0px; box-sizing:border-box; display:inline; height:auto; list-style:none; margin:0px; max-width:100%; opacity:1; outline:none; padding:0px; transition:all 0.4s ease-in-out; vertical-align:middle" title="(20, 8) \\longrightarrow f(x,y) = 20x + 8y = 20 \\cdot 20 + 8 \\cdot 8 = 464" /></p>\r\n\r\n<p>Nilai fungsi pada titik&nbsp;<img alt="(0, 20)" class="latex tie-appear" src="http://s0.wp.com/latex.php?latex=%280%2C+20%29&amp;bg=f9f9f9&amp;fg=000000&amp;s=0" style="border:0px; box-sizing:border-box; display:inline; height:auto; list-style:none; margin:0px; max-width:100%; opacity:1; outline:none; padding:0px; transition:all 0.4s ease-in-out; vertical-align:middle" title="(0, 20)" />&nbsp;dan&nbsp;<img alt="(20, 0)" class="latex tie-appear" src="http://s0.wp.com/latex.php?latex=%2820%2C+0%29&amp;bg=f9f9f9&amp;fg=000000&amp;s=0" style="border:0px; box-sizing:border-box; display:inline; height:auto; list-style:none; margin:0px; max-width:100%; opacity:1; outline:none; padding:0px; transition:all 0.4s ease-in-out; vertical-align:middle" title="(20, 0)" />&nbsp;tidak perlu dihitung, karena berada di bawah garis&nbsp;<img alt="2x + y = 48" class="latex tie-appear" src="http://s0.wp.com/latex.php?latex=2x+%2B+y+%3D+48&amp;bg=f9f9f9&amp;fg=000000&amp;s=0" style="border:0px; box-sizing:border-box; display:inline; height:auto; list-style:none; margin:0px; max-width:100%; opacity:1; outline:none; padding:0px; transition:all 0.4s ease-in-out; vertical-align:middle" title="2x + y = 48" />, yang mana nilainya pasti lebih rendah.</p>\r\n\r\n<p>Jadi, nilai maksimum fungsi objektif&nbsp;<img alt="f(x, y) = 20x + 8y" class="latex tie-appear" src="http://s0.wp.com/latex.php?latex=f%28x%2C+y%29+%3D+20x+%2B+8y&amp;bg=f9f9f9&amp;fg=000000&amp;s=0" style="border:0px; box-sizing:border-box; display:inline; height:auto; list-style:none; margin:0px; max-width:100%; opacity:1; outline:none; padding:0px; transition:all 0.4s ease-in-out; vertical-align:middle" title="f(x, y) = 20x + 8y" />&nbsp;program linear tersebut adalah 464.</p>\r\n', 3, '', 'publish', 1463141460, 1463141460, 6);

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE IF NOT EXISTS `status` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `name`, `create_time`, `update_time`) VALUES
(0, 'draf', NULL, NULL),
(1, 'publish', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subkategori`
--

CREATE TABLE IF NOT EXISTS `subkategori` (
  `id` int(11) NOT NULL,
  `kategori_id` int(11) DEFAULT '1',
  `nama` varchar(250) DEFAULT NULL,
  `deskripsi` text,
  `icon` varchar(1000) DEFAULT 'icon-custom icon-sm rounded-x icon-bg-green icon-line icon-graduation'
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subkategori`
--

INSERT INTO `subkategori` (`id`, `kategori_id`, `nama`, `deskripsi`, `icon`) VALUES
(1, 1, 'Pendidikan Agama', NULL, 'icon-custom icon-sm rounded-x icon-bg-green icon-line icon-graduation'),
(2, 1, 'Pendidikan Pancasila', NULL, 'icon-custom icon-sm rounded-x icon-bg-green icon-line icon-graduation'),
(3, 1, 'Matematika', NULL, 'icon-custom icon-sm rounded-x icon-bg-green icon-line icon-graduation'),
(4, 1, 'Sejarah Indonesia', NULL, 'icon-custom icon-sm rounded-x icon-bg-green icon-line icon-graduation'),
(5, 1, 'Bahasa Indonesia', NULL, 'icon-custom icon-sm rounded-x icon-bg-green icon-line icon-graduation'),
(6, 1, 'Bahasa Inggris', NULL, 'icon-custom icon-sm rounded-x icon-bg-green icon-line icon-graduation'),
(7, 1, 'Seni Budaya', NULL, 'icon-custom icon-sm rounded-x icon-bg-green icon-line icon-graduation'),
(8, 1, 'Prakarya', NULL, 'icon-custom icon-sm rounded-x icon-bg-green icon-line icon-graduation'),
(9, 1, 'Penjaskes', NULL, 'icon-custom icon-sm rounded-x icon-bg-green icon-line icon-graduation'),
(10, 1, 'Biologi', NULL, 'icon-custom icon-sm rounded-x icon-bg-green icon-line icon-graduation'),
(11, 1, 'Fisika', NULL, 'icon-custom icon-sm rounded-x icon-bg-green icon-line icon-graduation'),
(12, 1, 'Kimia', NULL, 'icon-custom icon-sm rounded-x icon-bg-green icon-line icon-graduation'),
(13, 1, 'Matematika', NULL, 'icon-custom icon-sm rounded-x icon-bg-green icon-line icon-graduation'),
(14, 1, 'Geografi', NULL, 'icon-custom icon-sm rounded-x icon-bg-green icon-line icon-graduation'),
(15, 1, 'Sejarah', NULL, 'icon-custom icon-sm rounded-x icon-bg-green icon-line icon-graduation'),
(16, 1, 'Sosiologi dan Anthropologi', NULL, 'icon-custom icon-sm rounded-x icon-bg-green icon-line icon-graduation'),
(17, 1, 'Ekonomi', NULL, 'icon-custom icon-sm rounded-x icon-bg-green icon-line icon-graduation'),
(18, 1, ' Bahasa dan Sastra Arab', NULL, 'icon-custom icon-sm rounded-x icon-bg-green icon-line icon-graduation'),
(19, 1, 'Bahasa dan Sastra Mandarin', NULL, 'icon-custom icon-sm rounded-x icon-bg-green icon-line icon-graduation');

-- --------------------------------------------------------

--
-- Table structure for table `tutorial`
--

CREATE TABLE IF NOT EXISTS `tutorial` (
  `id` int(11) NOT NULL,
  `judul` varchar(250) DEFAULT NULL,
  `file` varchar(500) NOT NULL,
  `thumb` varchar(500) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `tag` varchar(1000) DEFAULT NULL,
  `subkategori_id` int(11) DEFAULT NULL,
  `deskripsi` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `downloads` bigint(20) DEFAULT '0',
  `views` bigint(20) DEFAULT '0',
  `like` bigint(20) DEFAULT '0',
  `share` bigint(20) DEFAULT '0',
  `status` varchar(15) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `name` varchar(128) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tutorial`
--

INSERT INTO `tutorial` (`id`, `judul`, `file`, `thumb`, `user_id`, `tag`, `subkategori_id`, `deskripsi`, `downloads`, `views`, `like`, `share`, `status`, `created`, `modified`, `name`) VALUES
(1, 'Cara membuat slide presentasi', '', NULL, 6, NULL, 8, '<p>testing</p>\r\n', 0, 2, 0, 0, 'publish', '2016-05-18 05:55:15', '0000-00-00 00:00:00', 'wer'),
(9, 'testing user dan upload', 'uploads/tutorial/tutorial_9.zip', NULL, 6, NULL, 3, '<p>dasdasdasdasd</p>\r\n', 0, 4, 0, 0, 'publish', '2016-05-18 04:18:30', '0000-00-00 00:00:00', ''),
(10, 't4sting url', 'http://localhost/web/uploads/tutorial/tutorial_10.zip', NULL, 6, NULL, 4, '<p>dsadmengjankadlme</p>\r\n\r\n<p>&nbsp;dasdasdmks</p>\r\n', 0, 2, 0, 0, 'publish', '2016-05-18 05:11:57', '0000-00-00 00:00:00', ''),
(12, 'testing testing testing dan testing', 'http://localhost/web/uploads/tutorial/tutorial_12.zip', NULL, 6, NULL, 1, '<p>mengagagagada</p>\r\n', 0, 2, 0, 0, 'publish', '2016-05-18 13:50:44', '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nis` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `realname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `link_website` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `foto` text COLLATE utf8_unicode_ci NOT NULL,
  `telepon` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `show_full_profile` int(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `nis`, `realname`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`, `link_website`, `foto`, `telepon`, `show_full_profile`) VALUES
(6, 'admin', '13/354542/SV', 'admin', '1-JIe-d4zg8DxzS908v1yeferIuTQTK3', '$2y$13$ivvQwnZfflPNUn8e50JdTuckcUl0CIPoJk3J34ujySj2xGk1vaMqu', NULL, 'admin@mail.com', 10, 0, 0, 'buatwebsitemu.com', '6.png', '1616', 1),
(7, 'aditya', '13/354542/SV/04722', 'aditya rikky s', 'RJxuaTl2RhrypAiqxmAXfb3tB4QVSZse', '$2y$13$1CMNmFni/hbBRMumek0NROdxYoDl23g2fmjFad3IU6.UXt4y53Hle', NULL, 'admin@gmail.com', 10, 0, 0, 'www.buatwebsitemu.com', '', '087733720767', 0);

-- --------------------------------------------------------

--
-- Stand-in structure for view `user_auth`
--
CREATE TABLE IF NOT EXISTS `user_auth` (
`item_name` varchar(64)
,`username` varchar(255)
,`realname` varchar(255)
,`telepon` varchar(255)
);

-- --------------------------------------------------------

--
-- Structure for view `jumlah_post`
--
DROP TABLE IF EXISTS `jumlah_post`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `jumlah_post` AS select `category`.`name` AS `name`,count(`post`.`category_id`) AS `count` from (`post` join `category`) where (`category`.`id` = `post`.`category_id`) group by `post`.`category_id`;

-- --------------------------------------------------------

--
-- Structure for view `jumlah_post_byid`
--
DROP TABLE IF EXISTS `jumlah_post_byid`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `jumlah_post_byid` AS select `user`.`username` AS `username`,count(`post`.`id`) AS `count` from (`user` join `post`) where (`user`.`id` = `post`.`user_id`) group by `user`.`id`;

-- --------------------------------------------------------

--
-- Structure for view `jumlah_tutorial`
--
DROP TABLE IF EXISTS `jumlah_tutorial`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `jumlah_tutorial` AS select `subkategori`.`nama` AS `nama`,count(`tutorial`.`subkategori_id`) AS `count` from (`subkategori` join `tutorial`) where (`subkategori`.`id` = `tutorial`.`subkategori_id`) group by `tutorial`.`subkategori_id`;

-- --------------------------------------------------------

--
-- Structure for view `jumlah_tutorial_byid`
--
DROP TABLE IF EXISTS `jumlah_tutorial_byid`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `jumlah_tutorial_byid` AS select `user`.`username` AS `username`,count(`tutorial`.`id`) AS `count` from (`user` join `tutorial`) where (`user`.`id` = `tutorial`.`user_id`) group by `tutorial`.`user_id`;

-- --------------------------------------------------------

--
-- Structure for view `pdf_post`
--
DROP TABLE IF EXISTS `pdf_post`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `pdf_post` AS select `post`.`title` AS `name`,`category`.`name` AS `category`,`post`.`status` AS `status`,`user`.`realname` AS `realname`,`user`.`nis` AS `nis`,`user`.`email` AS `email`,`user`.`telepon` AS `telepon`,`post`.`user_id` AS `user_id` from ((`user` join `category`) join `post`) where ((`post`.`user_id` = `user`.`id`) and (`category`.`id` = `post`.`category_id`));

-- --------------------------------------------------------

--
-- Structure for view `pdf_tutorial`
--
DROP TABLE IF EXISTS `pdf_tutorial`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `pdf_tutorial` AS select `tutorial`.`judul` AS `name`,`subkategori`.`nama` AS `category`,`tutorial`.`status` AS `status`,`user`.`realname` AS `realname`,`user`.`nis` AS `nis`,`user`.`email` AS `email`,`user`.`telepon` AS `telepon`,`tutorial`.`user_id` AS `user_id` from ((`tutorial` join `user`) join `subkategori`) where ((`tutorial`.`user_id` = `user`.`id`) and (`tutorial`.`subkategori_id` = `subkategori`.`id`));

-- --------------------------------------------------------

--
-- Structure for view `user_auth`
--
DROP TABLE IF EXISTS `user_auth`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `user_auth` AS select `auth_assignment`.`item_name` AS `item_name`,`user`.`username` AS `username`,`user`.`realname` AS `realname`,`user`.`telepon` AS `telepon` from (`auth_assignment` join `user`) where (`user`.`id` = `auth_assignment`.`user_id`);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `type` (`type`);

--
-- Indexes for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Indexes for table `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengaturan`
--
ALTER TABLE `pengaturan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_post_category` (`category_id`),
  ADD KEY `FK_post_user` (`user_id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subkategori`
--
ALTER TABLE `subkategori`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kategori_fk_sub` (`kategori_id`);

--
-- Indexes for table `tutorial`
--
ALTER TABLE `tutorial`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tutorial_user_fk` (`user_id`),
  ADD KEY `subkategori_fk` (`subkategori_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `subkategori`
--
ALTER TABLE `subkategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `tutorial`
--
ALTER TABLE `tutorial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_assignment_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `post`
--
ALTER TABLE `post`
  ADD CONSTRAINT `post_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `post_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `subkategori`
--
ALTER TABLE `subkategori`
  ADD CONSTRAINT `subkategori_ibfk_1` FOREIGN KEY (`kategori_id`) REFERENCES `kategori` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `tutorial`
--
ALTER TABLE `tutorial`
  ADD CONSTRAINT `tutorial_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tutorial_ibfk_2` FOREIGN KEY (`subkategori_id`) REFERENCES `subkategori` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
