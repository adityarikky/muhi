<?php

namespace app\controllers;

use Yii;
use app\models\Subkategori;
use app\models\SubkategoriSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\filters\AccessControl;

/**
 * SubkategoriController implements the CRUD actions for Subkategori model.
 */
class SubkategoriController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','create','update','delete','getsubkategori'],
                'rules' => [
                    [
                        'actions' => ['index','create','update','delete','getsubkategori'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Subkategori models.
     * @return mixed
     */
    public function actionIndex()
    {
         if( Yii::$app->user->can('teacher')){

    
        $this->layout= 'adminpage';
        
        $searchModel = new SubkategoriSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
        
        }else{
            throw new ForbiddenHttpException;
            
        }
    }

    /**
     * Displays a single Subkategori model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if( Yii::$app->user->can('teacher')){

        
        $this->layout= 'adminpage';
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);

        }else{
            throw new ForbiddenHttpException;
            
        }
    }

    /**
     * Creates a new Subkategori model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if( Yii::$app->user->can('teacher')){

       
        $this->layout= 'adminpage';
        $model = new Subkategori();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }

         }else{
            throw new ForbiddenHttpException;
            
        }
    }

    /**
     * Updates an existing Subkategori model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if( Yii::$app->user->can('teacher')){

       
        $this->layout= 'adminpage';
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }

         }else{
            throw new ForbiddenHttpException;
            
        }
    }

    /**
     * Deletes an existing Subkategori model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if( Yii::$app->user->can('teacher')){

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
        }else{
            throw new ForbiddenHttpException;
            
        }
    }

    /**
     * Finds the Subkategori model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Subkategori the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Subkategori::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
